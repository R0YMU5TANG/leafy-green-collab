#Digital Clock
import time

#Python 2 & 3 support with try/except
try:
    #Python2
    import Tkinter as tk
    from Tkinter import *
except ImportError:
    #Python3
    import tkinter as tk
    from tkinter import *


def tick(time1=''):
    #get local time
    time2 = time.strftime('%I:%M:%S')
    #if time changes, update
    if time2 != time1:
        time1 = time2
        clock_frame.config(text=time2)
    #calls self every ~200ms
    clock_frame.after(200, tick)


root = tk.Tk()
root.title('Clock')
clock_frame = tk.Label(root, font=('times', 100, 'bold'), bg='black', fg='purple')
clock_frame.pack(fill='both', expand=1)
root.geometry('700x500')
tick()
root.mainloop()
