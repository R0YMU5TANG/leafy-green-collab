# Magic 8 Ball
import random

def question():
    messages = ['It is certain',
                'It is decidedly so',
                'Yes definitely',
                'Reply hazy try again',
                'Ask again later',
                'Concentrate and ask again',
                'My reply is no',
                'Outlook not so good',
                'Very doubtful']

    print('Ask a question')
    input()

    print(messages[random.randint(0, len(messages) - 1)])

    # prevent close and ask again prompt
    a=input('Do you have another question for the 8ball? yN')
    # ask again prompt handle
    if a.lower() == 'y':
        question()
    elif a.lower() == 'n':
        exit
    else: # lazy else handle. if not y or n close
        exit


# call func
question()
