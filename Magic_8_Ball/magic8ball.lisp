(defpackage #:mag8ball
  (:use :cl)
  (:export :ask-a-question))

(in-package mag8ball)

(defparameter *response*
  #("It is certain." "It is decidedly so" "Yes definitely" "Replay hazy try again" "Ask again later" "Concetrate and ask again" "My reply is no good" "Outlook not so good" "Very doubtful"))

(defun prompt (line)
  (format t "~a~%" line))

(defun ask-a-question ()
  (prompt "What is your question?")
  (read)
  (format t "~a~%" (aref *response* (random 9))))

(ask-a-question)
