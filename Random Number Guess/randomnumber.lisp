(defparameter solution 0)
(defvar guesses_taken 0)
  
(defun guess-num ()
  (setf solution (random 20))
  (format t "I'm thinking of a number between 1 and 20~%")
  (loop
     (let
	 ((guess (read)))
       (cond
	 ((<= guesses_taken 7)
	  (cond
	    ((< guess solution)
	     (format t "Your guess was too low. Go higher!~%")
	     (incf guesses_taken))
	    ((> guess solution)
	     (format t "Your guess was too high. Go lower!~%")
	     (incf guesses_taken))
	    ((equal guess solution)
	     (format t "Congratulations! You guessed the number in ~a guesses!~%" guesses_taken)
	     (setf guesses_taken 0)
	     (format t "Would you like to play again?~%")
	     (let
		 ((again (y-or-n-p)))
	       (cond
		 ((equal again T)
		  (guess-num))
		 ((equal again NIL)
		  (format t "Thanks for playing!~%")
		  (quit)))))))
	 ((> guesses_taken 7)
	  (format t "Nope. The number was ~a~%" solution)
	  (format t "Would you like to play again?~%")
	  (let
	      ((again (y-or-n-p)))
	    (cond
	      ((equal again T)
	       (guess-num))
	      ((equal again NIL)
	       (format t "Thanks for playing!~%")
	       (quit)))))))))
