# Random number guessing game
import random


def num_guess():
    number = random.randint(1, 20)
    print('I am thinking of a number betweeen 1 and 20')


    # Ask player to guess 6 times
    for guessTaken in range(1, 7):
        print('Take a guess!')
        guess = int(input())

        if guess < number:
            print('Your guess was too low. Go higher')
        elif guess > number:
            print('Your guess was too high. Go lower')
        else:
            break # Guess was correct

    if guess == number:
        print('Congrats! You guessed the number in ' + str(guessTaken) + ' guesses!')
    else:
        print('Nope. The number was ' + str(number))

    # play again?
    a=input('Would you like to play again? yN')
    if a.lower() == 'y':
        num_guess()
    else:
        exit # lazy exit


# call
num_guess()
