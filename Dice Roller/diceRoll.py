from random import randint



def main():
    print("""
          DICE ROLLER
          """)
    print("""
          OPTIONS:
          1: Coin
          2: 4-sided
          3: 6-sided
          4: 8-sided
          5: 10-sided
          6: 12-sided
          7: 20-sided
          8: Percentile Roll
          q: Quit
          """)


    choice = input("Which option are you rolling: ")

    repeat = True

    if choice == "1":
        print("COIN FLIP")
        while repeat:
            flip = randint(1,2)
            if (flip == 1):
                print("Heads")
                print("Do you want to flip again?")
            else:
                print("Tails")
                print("Do you want to flip again? Y/N")
            repeat = ("y" or "yes") in input().lower()
    elif choice == "2":
        print("ROLLING D4")
        while repeat:
            roll = randint(1,4)
            print("Rolled: ", roll)
            print("Do you want to roll again? Y/N")
            repeat = ("y" or "yes") in input().lower()
    elif choice == "3":
        print("ROLLING D6")
        while repeat:
            roll = randint(1,6)
            print("Rolled: ", roll)
            print("Do you want to roll again? Y/N")
            repeat = ("y" or "yes") in input().lower()
    elif choice == "4":
        print("ROLLING D8")
        while repeat:
            roll = randint(1,8)
            print("Rolled: ", roll)
            print("Do you want to roll again? Y/N")
            repeat = ("y" or "yes") in input().lower()
    elif choice == "5":
        print("ROLLING D10")
        while repeat:
            roll = randint(1,10)
            print("You rolled ",roll)
            print("Do you want to roll again? Y/N")
            repeat = ("y" or "yes") in input().lower()
    elif choice == "6":
        print("ROLLING D12")
        while repeat:
            roll = randint(1,12)
            print("You rolled ",roll)
            print("Do you want to roll again? Y/N")
            repeat = ("y" or "yes") in input().lower()
    elif choice == "7":
        print("ROLLING D20")
        while repeat:
            roll = randint(1,20)
            print("You rolled ",roll)
            print("Do you want to roll again? Y/N")
            repeat = ("y" or "yes") in input().lower()
    elif choice == "8":
        print("PERCENTILE ROLL")
        while repeat:
            per = randint(0,99)
            print("Your percentage is ", per)
            print("Do you want to roll another percentile? Y/N")
            repeat = ("y" or "yes") in input().lower()
    elif choice.lower() == "q":
        quit()
    else:
        print("Invalid input. Please choose a choice from the list")
        main()

while True:
    main()
    if input("Do you need to flip/roll another coin/die? Y/N").strip().lower() == "y":
        main()
    else:
        break
