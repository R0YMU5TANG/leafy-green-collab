# Leafy-Green-Collab

A collection of collaborative projects between Kaleb Sego and Will Sinatra.

Because Coding is Friendship.
 
PYTHON TEXT BASED ADVENTURE GAME

START()
Character Selection
    Choose class
    Assign attributes
        Health
        Mag
        Def
        Str
        Etc Etc
            Use Dice Roll Calculator for this? -> Turn into GUI so it's easier to use
    Name
    Etc 

Functions:
    Atk / dmg
    Enemies (list and attributes)
    Spells / dmg 
    Death
    Win 
    Items?
        Effects from items? Use while or for loops? i.e. strength gives +50% dmg for 3 turns
    Save
        Exports all relevant data to txt file for loading later?
        Allows loading of diff saves if done right 

Additional:
    Make gui? 