#!/usr/bin/env python

from tkinter import *


class Application(Frame):
    # GUI #

    def __init__(self, master):
        # initializes frame #
        Frame.__init__(self, master)
        self.grid()
        self.button_clicks = 0 # count button clicks
        self.create_widgets() # create widgets method


    def create_widgets(self):
        # creates widgets #
        self.btn1 = Button(self, text = "Total Clicks: 0")
        self.btn1["command"] = self.update_count # bind event handler
        self.btn1.grid()


    def update_count(self):
        # Increase click count, display total #
        self.button_clicks += 1
        self.btn1["text"] = "Total Clicks: " + str(self.button_clicks)


# tkinter variables
root = Tk()
root.title("GUI")
root.geometry("500x500")
app = Application(root)
root.mainloop()
