#!/usr/bin/env python

from tkinter import *


class Application(Frame):
    # GUI #

    def __init__(self, master):
        # initializes frame #
        Frame.__init__(self, master)
        self.grid()
        self.create_widgets() # create widgets method

    def create_widgets(self):
        # button, text, entry #
        self.instruction = Label(self, text = "Enter password")
        # row, column, span, sticky - NESW
        self.instruction.grid(row = 0, column = 0, columnspan = 2, sticky = W)

        # entry widget
        self.password = Entry(self)
        self.password.grid(row = 0, column = 1, sticky = W)

        # submit -> bind to reveal
        self.submit_button = Button(self, text = "Submit", command = self.reveal)
        self.submit_button.grid(row = 2, column = 0, sticky = W)

        # text -> width, height, wrap(WORD, CHAR, NONE, etc)
        self.text = Text(self, width = 35, height = 5, wrap = WORD)
        self.text.grid(row = 3, column = 0, columnspan = 2, sticky = W)


    def reveal(self):
        # Display message based on PW #
        content = self.password.get()

        if content == "password":
            message = "Success"
        else:
            message = "Failed"

            
        self.text.delete(0.0, END) # clears textbox
        self.text.insert(0.0, message) # 0.0(row.column) - position


# tkinter variables
root = Tk()
root.title("GUI - Entry")
root.geometry("500x500")
app = Application(root)
root.mainloop()
