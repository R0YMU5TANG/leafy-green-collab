#!/usr/bin/env python

from tkinter import *


class Application(Frame):
    # GUI #

    def __init__(self, master):
        # initializes frame #
        Frame.__init__(self, master)
        self.grid()
        self.create_widgets() # create widgets method


    def create_widgets(self):
        # radio buttons #
        Label(self, text = "Choose option").grid(row = 0, column = 0, sticky = W)

        Label(self, text = "Select one option").grid(row = 1, column = 0, sticky = W)

        # radio buttons
        self.option = StringVar()
        self.option.set("None")
        Radiobutton(self,
                    text = "option1",
                    value = "option1",
                    variable = self.option,
                    command = self.update_text).grid(row = 2, column = 0, sticky = W)
        Radiobutton(self,
                    text = "option2",
                    value = "option2",
                    variable = self.option,
                    command = self.update_text).grid(row = 3, column = 0, sticky = W)

        self.result = Text(self, width = 40, height = 5, wrap = WORD)
        self.result.grid(row = 5, column = 0, columnspan = 3)


    def update_text(self):
        # update text #
        message = ""
        message += self.option.get()

        self.result.delete(0.0, END)
        self.result.insert(0.0, message)
        


# tkinter variables
root = Tk()
root.title("GUI - Radio Buttons")
root.geometry("500x500")
app = Application(root)
root.mainloop()
