#Farenheit to Celcius converter
#input -> output


def doConversion():
    temp = input("Input the  temperature you would like to convert (e.g. 45F, 102C) : ")
    degree = int(temp[:-1])
    i_convention = temp[-1]

    if i_convention.upper() == "C":
        result = int(round((9 * degree) / 5 + 32))
        o_convention = "Fahrenheit"
    elif i_convention.upper() == "F":
        result = int(round((degree - 32) * 5 / 9))
        o_convention = "Celsius"
    else:
        print("Input proper convention.")


    print(temp, "is equal to", result, "degrees", o_convention)

    r=input('Convert more temperatures? yN')
    if r.lower() == 'y':
        doConversion()
    else: # lazy else
        quit()


doConversion()
